"""1. Comparar busqueda binaria, lineal y *if in* de una lista.
"""
from abc import ABCMeta, abstractmethod
import logging
from time import time

#===============CONFIGURACIÓN LOGGING======================

logging.basicConfig(filename="ejercicios_listas.log",
                    level=logging.DEBUG,
                    format="%(asctime)s - funcion: %(funcName)s - %(message)s"
                    )

#==========================================================



class metaBusquedas(metaclass=ABCMeta):
    @abstractmethod
    def busqueda_lineal(self):
        pass
    @abstractmethod
    def busqueda_binaria(self):
        pass
    @abstractmethod
    def busqueda_if_in(self):
        pass

class Busquedas(metaBusquedas):
    
    def __init__(self,lista,dato):
        self.lista=lista
        self.dato=dato
       
       
    def busqueda_lineal(self):
        self.t_inicio=time()

        self.busq=False

        for palabra in self.lista:
            if palabra==self.dato:
                self.indice=self.lista.index(palabra)
                self.busq=True
        
        if self.busq==True:
            self.t_transc=time()-self.t_inicio
            logging.debug(f"Búsqueda: {self.dato}, índice: {self.indice}, Tiempo ejec: {self.t_transc}")
            
        else:
            self.t_transc=time()-self.t_inicio
            logging.debug(f"Dato {self.dato} no se encuentra en lista. Tiempo ejec: {self.t_transc}")
            self.indice=-1
        
        return self.indice


    def busqueda_binaria(self):
        self.t_inicio=time()
        self.lista_0=0
        self.lista_n=len(self.lista)-1
        self.busq=False
        
        while(self.lista_0<=self.lista_n):
            self.pivote=int((self.lista_n+self.lista_0)/2)
            if self.lista[self.pivote]<self.dato:
                self.lista_0=self.pivote+1
            elif self.lista[self.pivote]>self.dato:
                self.lista_n=self.pivote-1
            else:
                self.indice=self.pivote
                self.t_transc=time()-self.t_inicio
                logging.debug(f"Búsqueda: {self.dato}, índice: {self.indice}, Tiempo ejec: {self.t_transc}")
                return self.indice 
        
            
       
        self.t_transc=time()-self.t_inicio
        logging.debug(f"Dato {self.dato} no se encuentra en lista. Tiempo ejec: {self.t_transc}")
        self.indice=-1
        
        return self.indice 




        pass
    def busqueda_if_in(self):
        self.t_inicio=time()
        if self.dato in self.lista:
            self.t_transc=time()-self.t_inicio
            logging.debug(f"Búsqueda: {self.dato}, índice: {self.lista.index(self.dato)}, Tiempo ejec: {self.t_transc}")
            self.indice=self.lista.index(self.dato)
    
        else:
            self.t_transc=time()-self.t_inicio
            logging.debug(f"Dato {self.dato} no se encuentra en lista. Tiempo ejec: {self.t_transc}")
            self.indice=-1
        
        return self.indice 
        

