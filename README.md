# Preguntas sobre Python
---
### Contenidos

- Algoritmos básicos
- Test unitarios (unittest)
- logs (logging)


### A continuación, se enumeran los archivos y preguntas básicas sobre conceptos aplicados en lenguaje Python.


#### preguntas_sobre_python.py

1. Escribir un programa para ejecutar el algoritmo de clasificación de burbujas.

2. Escribir un programa para producir un triángulo de estrellas

3. Escribir un programa que produzca la serie Fibonacci

4. Escribir un programa que cuente el número de letras mayúsculas en un archivo.

5. Escribir una función max_in_list() que tome una lista de números y devuelva el mas grande.

6. Escribir una función mas_larga() que tome una lista de palabras y devuelva la mas larga.

7. Crear una función contar_vocales(), que reciba una palabra y cuente cuantas letras "a" tiene, cuantas letras "e" tiene y así hasta completar todas las vocales.
Se puede hacer que el usuario sea quien elija la palabra.

#### ejercicios_listas.py

1. Comparar tiempos de ejecución de busqueda binaria, lineal y *if in* de una lista.
