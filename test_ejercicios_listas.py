import unittest
from ejercicios_listas import Busquedas

class TestBusquedas(unittest.TestCase):
    def setUp(self):
        self.lista_original=[var+1 for var in range(0,10000) ]
        self.dato=9999
        self.resultado=9998
        self.no_existe=10002
        self.no_encontrado=-1
        
        
    def test_busqueda_lineal(self):
        self.busqueda_1=Busquedas(self.lista_original,self.dato)
        self.assertEqual(self.busqueda_1.busqueda_lineal(),self.resultado)
        self.busqueda_2=Busquedas(self.lista_original,self.no_existe)
        self.assertEqual(self.busqueda_2.busqueda_lineal(),self.no_encontrado)
    def test_busqueda_if_in(self):
        self.busqueda_3=Busquedas(self.lista_original,self.dato)
        self.assertEqual(self.busqueda_3.busqueda_if_in(),self.resultado)
        self.busqueda_4=Busquedas(self.lista_original,self.no_existe)
        self.assertEqual(self.busqueda_4.busqueda_if_in(),self.no_encontrado)
    def test_busqueda_binaria(self):
        self.busqueda_5=Busquedas(self.lista_original,self.dato)
        self.assertEqual(self.busqueda_5.busqueda_binaria(),self.resultado)
        self.busqueda_6=Busquedas(self.lista_original,self.no_existe)
        self.assertEqual(self.busqueda_6.busqueda_binaria(),self.no_encontrado)

