import unittest
import preguntas_sobre_python


class TestAlgoritmoBurbuja(unittest.TestCase):        

    def test_num_distintos(self):
        self.lista=[1,2,7,4,3,2]
        self.resultado=[1,2,2,3,4,7]
        self.assertEqual(preguntas_sobre_python.algoritmo_burbuja(self.lista),self.resultado)

    def test_lista_ceros(self):
        self.lista_ceros=[0,0,0,0]
        self.resultado_ceros=[0,0,0,0]
        self.assertEqual(preguntas_sobre_python.algoritmo_burbuja(self.lista_ceros),self.resultado_ceros)

    def test_lista_vacia(self):
        self.lista_vacia=[]
        self.resultado_vacia=[]
        self.assertEqual(preguntas_sobre_python.algoritmo_burbuja(self.lista_vacia),self.resultado_vacia)


class TestFibonacci(unittest.TestCase):
   
   def test_fibonacci(self):
        self.limite_fibonacci=5
        self.resultado_fibonacci=[1,1,2,3,5]
        self.assertEqual(preguntas_sobre_python.fibonacci(self.limite_fibonacci),self.resultado_fibonacci)


class TestPalindromo(unittest.TestCase):
    def test_string_par(self):
        self.string_par="papapa"
        self.assertEqual(preguntas_sobre_python.palindromo(self.string_par),"string par")
    def test_no_palindromo(self):
        self.string_par="papapal"
        self.assertEqual(preguntas_sobre_python.palindromo(self.string_par),"no es palíndromo")
    def test_palindromo(self):
        self.string_par="papapap"
        self.assertEqual(preguntas_sobre_python.palindromo(self.string_par),"es palíndromo")



class TestMaxInList(unittest.TestCase):
    def test_lista(self):
        self.lista=[1,2,5,3]
        self.resultado=5
        self.assertEqual(preguntas_sobre_python.max_in_list(self.lista),self.resultado)
    
    def test_lista_iguales(self):
        self.lista=[2,2,2,2]
        self.resultado=2
        self.assertEqual(preguntas_sobre_python.max_in_list(self.lista),self.resultado)


class TestFuncionMasLarga(unittest.TestCase):
    def test_palabra_mas_larga(self):
        self.lista=["as","oso","vamolopibe","perro"]
        self.resultado="vamolopibe"

        self.assertEqual(preguntas_sobre_python.funcion_mas_larga(self.lista),self.resultado)

class TestFuncionMasLargaSimp(unittest.TestCase):
    def test_palabra_mas_larga(self):
        self.lista=["as","oso","vamolopibe","perro"]
        self.resultado="vamolopibe"

        self.assertEqual(preguntas_sobre_python.funcion_mas_larga_simp(self.lista),self.resultado)

    def test_None(self):
        self.lista_none=[None]
        self.resultado_none=None

        self.assertEqual(preguntas_sobre_python.funcion_mas_larga_simp(self.lista_none),self.resultado_none)


class TestContarVocales(unittest.TestCase):
    def test_contar_vocales(self):
        self.string="abarquillandomelos"
        self.resultado={"a":3,"e":1,"i":1,"o":2,"u":1}
        self.assertEqual(preguntas_sobre_python.contar_vocales(self.string),self.resultado)
    def test_sin_vocales(self):
        self.string="sphynx"
        self.resultado={"a":0,"e":0,"i":0,"o":0,"u":0}
        self.assertEqual(preguntas_sobre_python.contar_vocales(self.string),self.resultado)
    def test_numeros(self):
        self.string=1
        self.resultado={"a":0,"e":0,"i":0,"o":0,"u":0}
        self.assertEqual(preguntas_sobre_python.contar_vocales(self.string),self.resultado)
    def test_none(self):
        self.string=None
        self.resultado={"a":0,"e":0,"i":0,"o":0,"u":0}
        self.assertEqual(preguntas_sobre_python.contar_vocales(self.string),self.resultado)



if __name__=="__main__":
    unittest.main()