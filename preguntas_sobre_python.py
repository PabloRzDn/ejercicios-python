from distutils.debug import DEBUG
import logging

from pandas import pivot_table

logging.basicConfig(filename="preguntas_sobre_python.log",
                    level=logging.DEBUG,
                    format="%(asctime)s - funcion: %(funcName)s - %(message)s"
                    )


'''
1. Escribir un programa para ejecutar el algoritmo de clasificación de burbujas.

'''

def algoritmo_burbuja(lista_por_ordenar=[]):
    logging.debug("Lista por ordenar: {}".format(lista_por_ordenar))
    if not lista_por_ordenar:
        logging.debug("Lista vacía. Retorna lista vacía")
        return lista_por_ordenar

    elif lista_por_ordenar == [0 for i in lista_por_ordenar]:
        logging.debug("Lista de ceros. Retorna  lista de ceros")
        return lista_por_ordenar
    
    else:

        for i in range(len(lista_por_ordenar)-1):
            for j in range(len(lista_por_ordenar)-1):
                if lista_por_ordenar[j]>lista_por_ordenar[j+1]:
                    
                    menor, mayor=lista_por_ordenar[j+1], lista_por_ordenar[j]
                    lista_por_ordenar[j], lista_por_ordenar[j+1]=menor, mayor
                    

        logging.debug("Lista ordenada: {}".format(lista_por_ordenar))    
        return lista_por_ordenar


'''
2. Escribir un programa para producir un triángulo de estrellas

'''


def triangulo_de_estrellas(numero_de_estratos):
    numero_de_estratos+=1
    for i in range(numero_de_estratos):
        print(" "*(numero_de_estratos-(i+1)),"*"*(2*i-1))


'''
3. Escribir un programa que produzca la serie Fibonacci
'''


def fibonacci(limite_fib):
    resultado=[]
    f_0=0
    f_1=1
    for i in range(limite_fib):
        f_fib=f_0+f_1
        f_0, f_1=f_1,f_fib
        resultado.append(f_0)
    logging.info("Resultado: {}".format(resultado))        
    return resultado



'''
4. Escribir un programa que compruebe que una secuencia es palíndromo

'''


def palindromo(secuencia):

    secuencia_invertida=secuencia[::-1]
    if len(secuencia)%2!=0:
        pivote=int((len(secuencia)-1)/2)
        
        for i in range(pivote):
            if secuencia[i]!=secuencia_invertida[i]:
                palindromo=False
                break
            else:
                palindromo=True
            
        if palindromo==False:
            logging.debug("'{}' no es palíndromo".format(secuencia))
            return "no es palíndromo"
        else:
            logging.debug("'{}' es palíndromo con pivote en '{}'".format(secuencia,secuencia[pivote]))
            return "es palíndromo"

    else:
        logging.debug("'{}' es un string par".format(secuencia))
        return "string par"
def contador_mayusculas(frase):
 
    contador=0

    for letra in range(len(frase)):
        if frase[letra]==frase[letra].upper() and frase[letra]!=" ":
            contador=contador+1

    print("La frase:",frase,"tiene",contador,"mayúsculas")
            
"""
 Escribir una función max_in_list() que tome una lista de números y devuelva el mas grande.
"""           

def max_in_list(lista=[]):
    for i in range(len(lista)-1):
        if lista[i+1]>lista[i]:
                max=lista[i+1]
        else:
            max=lista[i]
    logging.debug("lista: {}, número mayor: {}".format(lista,max))
    return max


"""
Escribir una función mas_larga() que tome una lista de palabras y devuelva la mas larga
"""

def funcion_mas_larga(lista_palabras=[]):
    for i in range(len(lista_palabras)):
        if len(lista_palabras[i-1]) > len(lista_palabras[i]):
            max=lista_palabras[i-1]
        else:
            max=lista_palabras[i]
    logging.debug("Lista: {}, palabra más larga: '{}'".format(lista_palabras,max))

    return max

def funcion_mas_larga_simp(lista_palabras=[]):
    if lista_palabras is not None:
        larga=max(lista_palabras)
        logging.debug("Lista: {}, palabra más larga: '{}'".format(lista_palabras,larga))
        return larga
    

def contar_vocales(string):

    try:
        vocales={"a":0, "e":0, "i":0, "o":0, "u":0}

        for letra in string:
            if letra in vocales.keys():
                vocales[letra]+=1
        
        logging.debug("String: {}, vocales: {}".format(string,vocales))
        return vocales
    except ValueError as v:
        logging.debug(f"Error: {v}, retorna {vocales}")
        return vocales
    except TypeError as t:
        logging.debug(f"Error: {t}, retorna {vocales}" )
        return vocales